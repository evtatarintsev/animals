# -*- coding: utf-8 -*-
__author__ = 'evtatarintsev'
from django.contrib import admin
from django.db import models
from django.contrib.flatpages.models import FlatPage
from django.contrib.flatpages.admin import FlatPageAdmin as DjangoFlatPageAdmin

from ckeditor.fields import CKEditorWidget


class FlatPageAdmin(DjangoFlatPageAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget}
    }

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageAdmin)