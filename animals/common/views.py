# -*- coding: utf-8 -*-
__author__ = 'Evgeniy Tatarintsev'
from django import http
from django.utils.http import is_safe_url
from django.utils.translation import check_for_language
from django.conf import settings

import urlparse


def set_language(request):
    next = request.META.get('HTTP_REFERER')
    if not is_safe_url(url=next, host=request.get_host()):
        next = '/'

    path = urlparse.urlparse(next).path

    for language in settings.LANGUAGES:
        if path.startswith('/'+language[0]+'/'):
            path = path[3:]

    response = http.HttpResponseRedirect(urlparse.urljoin(next, path))
    if request.method == 'GET':
        lang_code = request.GET.get('language', None)
        if lang_code and check_for_language(lang_code):
            if hasattr(request, 'session'):
                request.session['django_language'] = lang_code
            else:
                response.set_cookie(settings.LANGUAGE_COOKIE_NAME, lang_code)
    return response