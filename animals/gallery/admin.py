# -*- coding: utf-8 -*-
__author__ = 'evtatarintsev'
from django.contrib import admin
from modeltranslation.admin import TranslationAdmin

from sorl.thumbnail import get_thumbnail
from models import Animal, Image


class AnimalAdmin(TranslationAdmin):
    pass


class ImageAdmin(TranslationAdmin):
    list_filter = ('animal', )
    list_display = ('admin_image', 'animal', 'pubilshed',)

    def admin_image(self, obj):
        img = get_thumbnail(obj.image, "100", crop="center")
        return '<a href="{0}/"><img src="{1}"></a>'.format(obj.pk, img.url)
    admin_image.allow_tags = True
    admin_image.short_description = u'Изображение'


admin.site.register(Animal, AnimalAdmin)
admin.site.register(Image, ImageAdmin)