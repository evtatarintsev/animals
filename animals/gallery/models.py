# -*- coding: utf-8 -*-
__author__ = 'evtatarintsev'
import uuid
import os.path

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse


def image_upload_to(instance, filename):
    ext = filename.split('.')[-1]
    filename = '%s.%s'%(uuid.uuid4(), ext)
    return os.path.join('animals', instance.animal.slug, filename)


class AnimalManager(models.Manager):
    def popular(self):
        return self.order_by('rating')

    def get_for_menu(self):
        popular = self.popular()[:5]
        return popular, self.exclude(pk__in=[a.pk for a in popular])



class Animal(models.Model):
    name = models.CharField(_('Name'), max_length=50)
    slug = models.SlugField()
    rating = models.PositiveIntegerField(_('Rating'), default=0)
    pubilshed = models.BooleanField(default=True)

    objects = AnimalManager()

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("animal-images", kwargs={'slug': self.slug})


class Image(models.Model):
    animal = models.ForeignKey(Animal, verbose_name=_('Animal'), related_name='images')
    alt = models.CharField('Alt', max_length=100, null=True, blank=True)
    image = models.ImageField(_('Image'), upload_to=image_upload_to)
    pubilshed = models.BooleanField(default=True)
