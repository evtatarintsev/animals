# -*- coding: utf-8 -*-
__author__ = 'evtatarintsev'
from modeltranslation.translator import translator, TranslationOptions

from models import Animal, Image


class AnimalTranslation(TranslationOptions):
    """Translation for country"""
    fields = ('name',)


class ImageTranslation(TranslationOptions):
    """Translation for region"""
    fields = ('alt',)


translator.register(Animal, AnimalTranslation)
translator.register(Image, ImageTranslation)
