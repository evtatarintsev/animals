# -*- coding: utf-8 -*-
__author__ = 'evtatarintsev'
import os
from django.views.generic import TemplateView, DetailView
from django.http import HttpResponse
from django.core.files import File
from django.conf import settings

from models import Image, Animal


class HomePageView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        ctx = super(HomePageView, self).get_context_data(**kwargs)
        ctx['images'] = Image.objects.all().order_by("?")[:100]
        ctx['menu_popular'], ctx['menu_items'] = Animal.objects.get_for_menu()
        return ctx


class AnimalImagesView(DetailView):
    template_name = 'index.html'
    model = Animal

    def get_context_data(self, **kwargs):
        ctx = super(AnimalImagesView, self).get_context_data(**kwargs)
        ctx['images'] = Image.objects.filter(animal=self.get_object())
        ctx['menu_popular'], ctx['menu_items'] = Animal.objects.get_for_menu()
        return ctx


class ChooseAnimalView(TemplateView):
    template_name = 'choose_animal.html'

    def get_context_data(self, **kwargs):
        ctx = super(ChooseAnimalView, self).get_context_data(**kwargs)
        ctx['images'] = Image.objects.all().order_by("?")[:100]
        ctx['menu_popular'], ctx['menu_items'] = Animal.objects.get_for_menu()
        return ctx


class AddImagesView(DetailView):
    template_name = 'add_images.html'
    model = Animal

    def get_context_data(self, **kwargs):
        ctx = super(AddImagesView, self).get_context_data(**kwargs)
        ctx['images'] = Image.objects.filter(animal=self.get_object())
        ctx['menu_popular'], ctx['menu_items'] = Animal.objects.get_for_menu()
        return ctx

    def post(self, request, *args, **kwargs):
        for f in request.FILES.getlist('Filedata'):
            file_path = os.path.join(settings.FILE_UPLOAD_TEMP_DIR, f.name)
            with open(file_path, 'wb+') as destination:
                for chunk in f.chunks():
                    destination.write(chunk)

                Image.objects.create(animal=self.get_object(), image=File(destination))
                os.remove(file_path)

        return HttpResponse('ok')
