from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.conf import settings

from animals.gallery.views import HomePageView, AnimalImagesView, AddImagesView, ChooseAnimalView

admin.autodiscover()


urlpatterns = patterns('',
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^i18n/setlang/$', 'animals.common.views.set_language', name='set_language'),
)

urlpatterns += i18n_patterns('',
    url(r'^$', HomePageView.as_view(), name='home-page'),
    url(r'^add-images/$', ChooseAnimalView.as_view(), name='choose-animal'),
    url(r'^add-images/(?P<slug>[-a-zA-Z0-9_]+)/$', AddImagesView.as_view(), name='add-images'),
    url(r'^(?P<slug>[-a-zA-Z0-9_]+)/$', AnimalImagesView.as_view(), name='animal-images'),
)


if settings.DEBUG:
    urlpatterns += patterns("",
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
            'show_indexes': True}),
    )